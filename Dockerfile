FROM golang@sha256:b11bad2ef5ef90ab7e5589d9a5af51bc3f65335278e73f95b18db2057c0505ae

RUN apt-get update && apt-get install -y \
    upx-ucl python3 \
 && rm -rf /var/lib/apt/lists/*

COPY genversion.py /root/genversion.py
RUN chmod a+x /root/genversion.py







