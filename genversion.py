#!/usr/bin/env python3
import sys, os

if __name__ == "__main__":
    sha = os.getenv("CI_COMMIT_SHORT_SHA", None)
    tag = os.getenv("CI_COMMIT_TAG", None)
    baseDir = os.getenv("CI_PROJECT_DIR")
    with open(baseDir + "/" + sys.argv[1]) as fd:
        versionTemplate = fd.read()
    versionTemplate.replace('var sha = "None"', f'var sha="{sha}"')
    versionTemplate.replace('var tag = "HEAD"', f'var tag = "{tag}"')
    with open(baseDir + "/" + sys.argv[1], 'w') as fd:
        fd.write(versionTemplate)
